"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = __importStar(require("../settings"));
var validators_1 = require("../services/validators");
var database_1 = require("../services/database");
var Helper = __importStar(require("../helper"));
var legacy = require('legacy-encoding');
var fs = require('fs');
function getLatest(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var data, cached, _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    data = null;
                    return [4 /*yield*/, database_1.redisRepository.get('banners:latest')];
                case 1:
                    cached = _d.sent();
                    if (!cached) return [3 /*break*/, 2];
                    _a = data = cached;
                    return [3 /*break*/, 5];
                case 2:
                    _c = (_b = database_1.redisRepository).set;
                    return [4 /*yield*/, database_1.bannerRepository.getLatestBanners()];
                case 3: return [4 /*yield*/, _c.apply(_b, [_d.sent(), 'banners:latest'])];
                case 4:
                    _a = data = _d.sent();
                    _d.label = 5;
                case 5:
                    _a;
                    res.status(200).json(data);
                    return [2 /*return*/];
            }
        });
    });
}
exports.getLatest = getLatest;
function getPreview(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, previewData, cached, _b, _c, _d;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0:
                    _a = req;
                    return [4 /*yield*/, validators_1.validateGetData(req)];
                case 1:
                    _a.getData = _e.sent();
                    previewData = null;
                    return [4 /*yield*/, database_1.redisRepository.get("banners:preview:" + req.getData.key)];
                case 2:
                    cached = _e.sent();
                    if (!cached) return [3 /*break*/, 3];
                    _b = previewData = cached;
                    return [3 /*break*/, 6];
                case 3:
                    _d = (_c = database_1.redisRepository).set;
                    return [4 /*yield*/, database_1.bannerRepository.getBannersPreviewData(req.getData.key)];
                case 4: return [4 /*yield*/, _d.apply(_c, [previewData = _e.sent(), "banners:preview:" + req.getData.key])];
                case 5:
                    _b = _e.sent();
                    _e.label = 6;
                case 6:
                    _b;
                    res.status(200).json({ previewData: previewData });
                    return [2 /*return*/];
            }
        });
    });
}
exports.getPreview = getPreview;
function preparePreview(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var previewId;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!!req.postData.items) return [3 /*break*/, 1];
                    Settings.logError('Invalid item data for banners preview generation.');
                    res.status(400).send();
                    return [3 /*break*/, 3];
                case 1: return [4 /*yield*/, database_1.bannerRepository.getBannersPreview(req.postData.items)];
                case 2:
                    previewId = _a.sent();
                    res.status(200).json({ preview: previewId });
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.preparePreview = preparePreview;
function getAdScript(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, _b, cachedItem, item, _c, _d, _e, cid, frameId, script;
        return __generator(this, function (_f) {
            switch (_f.label) {
                case 0:
                    _a = req;
                    return [4 /*yield*/, validators_1.validateReqParams(req)];
                case 1:
                    _a.paramData = _f.sent();
                    _b = req;
                    return [4 /*yield*/, validators_1.validateGetData(req)];
                case 2:
                    _b.getData = _f.sent();
                    if (!req.paramData.id) return [3 /*break*/, 9];
                    return [4 /*yield*/, database_1.redisRepository.get("banners:item:" + req.paramData.id)];
                case 3:
                    cachedItem = _f.sent();
                    if (!cachedItem) return [3 /*break*/, 4];
                    _c = cachedItem;
                    return [3 /*break*/, 7];
                case 4:
                    _e = (_d = database_1.redisRepository).set;
                    return [4 /*yield*/, database_1.bannerRepository.getBannerById(req.paramData.id)];
                case 5: return [4 /*yield*/, _e.apply(_d, [_f.sent(), "banners:item:" + req.paramData.id])];
                case 6:
                    _c = _f.sent();
                    _f.label = 7;
                case 7:
                    item = _c;
                    cid = Helper.generateSaferUniqueID();
                    frameId = "tag1_ad_" + cid;
                    script = "  var currpage = document.referrer;            \n                        console.log('Referrer: ' + currpage);\n\n                        var css = '#" + frameId + " { border: none; overflow: hidden; width: " + item.width + "px; height: " + item.height + "px; }';\n                        var style = document.createElement('style');\n                        style.type = 'text/css';\n                        if (style.styleSheet){\n                            // This is required for IE8 and below.\n                            style.styleSheet.cssText = css;\n                        } else {\n                            style.appendChild(document.createTextNode(css));\n                        }\n\n                        var frame = document.createElement('iframe');\n                        frame.id = '" + frameId + "';\n                        frame.srcdoc = '<!DOCTYPE html><html><head><title>TAG1</title><base href=\"" + Settings.API_BASE_URL + "banners/" + cid + "/files/" + req.paramData.id + "/\"><style>body { margin: 0; padding: 0; }</style></head><body><script src=\"" + Settings.API_BASE_URL + "banners/" + cid + "/" + req.paramData.id + ".js?ref='+currpage+'\"></script></body></html>';\n                    \n                        document.body.appendChild(style);\n                        document.getElementById('item" + req.paramData.id + "').appendChild(frame);";
                    return [4 /*yield*/, database_1.bannerRepository.setClickURL(cid, req.getData.c)];
                case 8:
                    if (_f.sent()) {
                        res.status(200).type('.js').send(script);
                    }
                    else {
                        Settings.logError('Failed to Register to Redis.');
                        res.status(400).send();
                    }
                    return [3 /*break*/, 10];
                case 9:
                    Settings.logError('Invalid item id for banner script.');
                    res.status(400).send();
                    _f.label = 10;
                case 10: return [2 /*return*/];
            }
        });
    });
}
exports.getAdScript = getAdScript;
function getAdContent(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var data, json, url, content, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0: return [4 /*yield*/, validators_1.validateReqParams(req)];
                case 1:
                    data = _c.sent();
                    return [4 /*yield*/, validators_1.validateJSONData(req)];
                case 2:
                    json = _c.sent();
                    if (!(data.id && data.link)) return [3 /*break*/, 6];
                    Settings.logInfo("Printing Banner: " + data.id + " with CID: " + data.link);
                    return [4 /*yield*/, database_1.bannerRepository.getClickURL(data.link)];
                case 3:
                    url = _c.sent();
                    _b = (_a = Helper).readFile;
                    return [4 /*yield*/, getClosestHTML("./files/" + data.id + "/")];
                case 4: return [4 /*yield*/, _b.apply(_a, [_c.sent()]).catch(function (err) {
                        Settings.logError(err);
                        res.status(400).send();
                        return;
                    })];
                case 5:
                    content = _c.sent();
                    if (content) {
                        res.status(200).type('.js').send("\n                document.open(\"text/html\", \"replace\")\n                document.write(\"<style>*{ overflow: -moz-scrollbars-none; overflow: hidden; } *::-webkit-scrollbar { width: 0 !important } .click_frame { cursor: pointer; position: absolute; z-index: 9999; width: 100%; height: 100%; left: 0; top: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; border: 1px solid #000; overflow: hidden; }</style>\");\n                document.write(\"" + Helper.safeJavascript(content) + "\");\n                document.write(\"<a target=\\\"_blank\\\"><div class=\\\"click_frame\\\" onClick=\\\"window.open('" + url + "', '_blank')\\\"></div></a>\")\n                document.close();\n            ");
                    }
                    _c.label = 6;
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.getAdContent = getAdContent;
function getAdElement(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var data, json, originalFile, file, options;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, validators_1.validateReqParams(req)];
                case 1:
                    data = _a.sent();
                    return [4 /*yield*/, validators_1.validateJSONData(req)];
                case 2:
                    json = _a.sent();
                    if (!(data.id && data.link && data.file)) return [3 /*break*/, 4];
                    originalFile = "./files/" + data.id + "/" + data.file;
                    return [4 /*yield*/, getClosestElementByName("./files/" + data.id + "/", data.file)];
                case 3:
                    file = _a.sent();
                    options = {
                        root: process.cwd() + '/',
                        dotfiles: 'deny',
                        headers: {
                            'x-timestamp': Date.now(),
                            'x-sent': true
                        }
                    };
                    res.sendFile(file, options);
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.getAdElement = getAdElement;
function getClosestElementByName(dir, filename) {
    return __awaiter(this, void 0, void 0, function () {
        var element, files;
        return __generator(this, function (_a) {
            element = "" + dir + filename;
            files = fs.readdirSync(dir);
            files.forEach(function (file, index) {
                var r = Helper.getSafeRegexp(filename);
                if (r.test(file)) {
                    element = "" + dir + file;
                    return element;
                }
            });
            return [2 /*return*/, element];
        });
    });
}
function getClosestHTML(dir) {
    return __awaiter(this, void 0, void 0, function () {
        var element, files;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    element = dir + "index.html";
                    files = fs.readdirSync(dir);
                    return [4 /*yield*/, files.forEach(function (file, index) {
                            var r = new RegExp(/(.*).html/);
                            if (r.test(file)) {
                                element = "" + dir + file;
                                return element;
                            }
                        })];
                case 1:
                    _a.sent();
                    return [2 /*return*/, element];
            }
        });
    });
}
function updateInfo(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var id, desc, update, suc;
        return __generator(this, function (_a) {
            id = req.postData.id, desc = req.postData.description;
            update = database_1.DB.updateFields("banners", ["descricao"], [desc], id);
            suc = false;
            if (update) {
                suc = true;
            }
            res.status(200).json({ success: suc });
            return [2 /*return*/];
        });
    });
}
exports.updateInfo = updateInfo;
function archiveItems(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var archive, redis;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!!req.postData.items) return [3 /*break*/, 1];
                    Settings.logError('Invalid item data for banners archive.');
                    res.status(400).send();
                    return [3 /*break*/, 4];
                case 1: return [4 /*yield*/, database_1.bannerRepository.archiveItems(req.postData.items)];
                case 2:
                    archive = _a.sent();
                    return [4 /*yield*/, database_1.redisRepository.deleteWhere('banners:*')];
                case 3:
                    redis = _a.sent();
                    res.status(200).json({ success: archive });
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.archiveItems = archiveItems;
//# sourceMappingURL=banners.js.map