"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var gwd_1 = __importDefault(require("./gwd"));
var glb_1 = __importDefault(require("./glb"));
var ada_1 = __importDefault(require("./ada"));
var Handlers = [gwd_1.default, glb_1.default, ada_1.default];
exports.default = Handlers;
//# sourceMappingURL=index.js.map