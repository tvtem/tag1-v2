"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = __importStar(require("../../settings"));
var handlers_1 = require("./handlers");
var database_1 = require("../../services/database");
var multer = require('multer');
var crypto = require("crypto");
var JSZip = require("jszip");
var fs = require("fs");
function isValidExt(ext) {
    switch (ext) {
        case "rar":
        case "zip": return true;
        default: return false;
    }
}
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './uploads/banners');
    },
    filename: function (req, file, callback) {
        var id = Date.now() + '' + crypto.randomBytes(8).toString("hex");
        var type = file.originalname.split('.');
        var ext = type[type.length - 1];
        file.type = ext;
        if (!isValidExt(ext)) {
            file.error = "INVALID_FILE";
        }
        file.id = id + '.' + ext;
        file.bannerId = id;
        callback(null, file.id);
    }
});
var BannerUpload = multer({ storage: storage }).single('file');
exports.BannerUpload = BannerUpload;
function ExtractZip(zip, zipf, dir) {
    return __awaiter(this, void 0, void 0, function () {
        var file, _loop_1, i;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    file = 'index.html';
                    _loop_1 = function (i) {
                        var filename, regex;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    filename = Object.keys(zipf.files)[i];
                                    return [4 /*yield*/, zip.file(filename).async('nodebuffer').then(function (content) {
                                            var dest = "" + dir + filename;
                                            if (!fs.existsSync(dir))
                                                fs.mkdirSync(dir);
                                            fs.writeFileSync(dest, content);
                                        })];
                                case 1:
                                    _a.sent();
                                    regex = new RegExp(/(.*).html/);
                                    if (regex.test(filename)) {
                                        file = filename;
                                        return [2 /*return*/, "continue"];
                                    }
                                    return [2 /*return*/];
                            }
                        });
                    };
                    i = 0;
                    _a.label = 1;
                case 1:
                    if (!(i < Object.keys(zipf.files).length)) return [3 /*break*/, 4];
                    return [5 /*yield**/, _loop_1(i)];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3:
                    i++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/, "" + dir + file];
            }
        });
    });
}
function UploadBanners(req, res, err) {
    return __awaiter(this, void 0, void 0, function () {
        var zip, fileObj;
        return __generator(this, function (_a) {
            zip = new JSZip();
            fileObj = null;
            try {
                // read a zip file
                fs.readFile("uploads/banners/" + req.file.id, function (err, data) {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (!(err || req.file.error)) return [3 /*break*/, 1];
                                    res.json({ success: false, error: req.file.error });
                                    return [3 /*break*/, 3];
                                case 1: return [4 /*yield*/, zip.loadAsync(data).then(function (zipf) {
                                        return __awaiter(this, void 0, void 0, function () {
                                            var dir, index, insert, redis;
                                            return __generator(this, function (_a) {
                                                switch (_a.label) {
                                                    case 0:
                                                        dir = "files/" + req.file.bannerId + "/";
                                                        return [4 /*yield*/, ExtractZip(zip, zipf, dir)];
                                                    case 1:
                                                        index = _a.sent();
                                                        if (!!fs.existsSync(index)) return [3 /*break*/, 2];
                                                        req.file.error = 'INVALID_FILE';
                                                        return [3 /*break*/, 6];
                                                    case 2: return [4 /*yield*/, handlers_1.HandleAdFile(index, req.file.bannerId, req.file.size, req.userId)];
                                                    case 3:
                                                        fileObj = _a.sent();
                                                        return [4 /*yield*/, database_1.bannerRepository.registerBanner(fileObj)];
                                                    case 4:
                                                        insert = _a.sent();
                                                        return [4 /*yield*/, database_1.redisRepository.deleteWhere('banners:*')];
                                                    case 5:
                                                        redis = _a.sent();
                                                        if (!insert)
                                                            req.file.error = 'DATABASE_ERROR';
                                                        _a.label = 6;
                                                    case 6:
                                                        res.json({ success: req.file.error ? false : true, id: req.file.bannerId, error: req.file.error ? req.file.error : null });
                                                        return [2 /*return*/];
                                                }
                                            });
                                        });
                                    })];
                                case 2:
                                    _a.sent();
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                });
            }
            catch (err) {
                Settings.logError(err.message);
            }
            return [2 /*return*/];
        });
    });
}
exports.UploadBanners = UploadBanners;
//# sourceMappingURL=file.js.map