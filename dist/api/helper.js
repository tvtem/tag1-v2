"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var nanoid = require('nanoid');
var generate = require('nanoid/generate');
var fs_1 = __importDefault(require("fs"));
var Settings = __importStar(require("./settings"));
function generateSafeUniqueID() {
    return nanoid(64);
}
exports.generateSafeUniqueID = generateSafeUniqueID;
function generateSaferUniqueID() {
    return generate('1234567890abcdefghijklmnopqrstuvwxyz_', 32);
}
exports.generateSaferUniqueID = generateSaferUniqueID;
function readFile(filename, encoding) {
    if (encoding === void 0) { encoding = 'utf8'; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                        try {
                            fs_1.default.readFile(filename, encoding, function (err, data) {
                                if (err) {
                                    reject(err);
                                }
                                else
                                    resolve(data);
                            });
                        }
                        catch (err) {
                            Settings.logError(err);
                            reject('Could not read the file at ' + filename + '.');
                        }
                    })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.readFile = readFile;
function safeJavascript(content) {
    content = content.replace(new RegExp('{{CLICK_TAG}}', 'g'), "");
    content = content.replace(new RegExp(';', 'g'), "; ");
    content = content.replace(/(\/\*([\s\S]*?)\*\/)|([^(?!:)]\/\/(.*)$)/gm, ' ');
    content = content.replace(/(\r\n|\n|\r)/gm, "\\n");
    content = content.replace(new RegExp('"', 'g'), '\\"');
    return content;
}
exports.safeJavascript = safeJavascript;
function getSafeRegexp(str) {
    var regexp = str;
    var oregexp = regexp;
    var jokers = "\u00E1\u00E9\u00ED\u00F3\u00FA\u00C1\u00C9\u00CD\u00D3\u00DA\u00E0\u00E8\u00EC\u00F2\u00F9\u00C0\u00C8\u00CC\u00D2\u00D9\u00E3\u00C3\u00E7\u00C7\u00FC\u00DC\u00EF\u00CF\u00E2\u00C2\u00EA\u00CA\u00EE\u00CE\u00F4\u00D4\u00FB\u00DB\u00E4\u00C4\u00EB\u00CB\u00F6\u00D6\u00F5\u00D5\u00F1\u00D1\u00FF\u00FD\u00DD";
    var jk = jokers.split('').forEach(function (c, index) {
        regexp = regexp.replace(new RegExp(c, 'g'), "(.*)");
    });
    return new RegExp(regexp, "g");
}
exports.getSafeRegexp = getSafeRegexp;
/**
 * Encodes multi-byte Unicode string into utf-8 multiple single-byte characters
 * (BMP / basic multilingual plane only).
 *
 * Chars in range U+0080 - U+07FF are encoded in 2 chars, U+0800 - U+FFFF in 3 chars.
 *
 * Can be achieved in JavaScript by unescape(encodeURIComponent(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} unicodeString - Unicode string to be encoded as UTF-8.
 * @returns {string} UTF8-encoded string.
 */
function utf8Encode(unicodeString) {
    if (typeof unicodeString != 'string')
        throw new TypeError('parameter ‘unicodeString’ is not a string');
    var utf8String = unicodeString.replace(/[\u0080-\u07ff]/g, // U+0080 - U+07FF => 2 bytes 110yyyyy, 10zzzzzz
    function (c) {
        var cc = c.charCodeAt(0);
        return String.fromCharCode(0xc0 | cc >> 6, 0x80 | cc & 0x3f);
    }).replace(/[\u0800-\uffff]/g, // U+0800 - U+FFFF => 3 bytes 1110xxxx, 10yyyyyy, 10zzzzzz
    function (c) {
        var cc = c.charCodeAt(0);
        return String.fromCharCode(0xe0 | cc >> 12, 0x80 | cc >> 6 & 0x3F, 0x80 | cc & 0x3f);
    });
    return utf8String;
}
exports.utf8Encode = utf8Encode;
/**
 * Decodes utf-8 encoded string back into multi-byte Unicode characters.
 *
 * Can be achieved JavaScript by decodeURIComponent(escape(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} utf8String - UTF-8 string to be decoded back to Unicode.
 * @returns {string} Decoded Unicode string.
 */
function utf8Decode(utf8String) {
    if (typeof utf8String != 'string')
        throw new TypeError('parameter ‘utf8String’ is not a string');
    // note: decode 3-byte chars first as decoded 2-byte strings could appear to be 3-byte char!
    var unicodeString = utf8String.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, // 3-byte chars
    function (c) {
        var cc = ((c.charCodeAt(0) & 0x0f) << 12) | ((c.charCodeAt(1) & 0x3f) << 6) | (c.charCodeAt(2) & 0x3f);
        return String.fromCharCode(cc);
    }).replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, // 2-byte chars
    function (c) {
        var cc = (c.charCodeAt(0) & 0x1f) << 6 | c.charCodeAt(1) & 0x3f;
        return String.fromCharCode(cc);
    });
    return unicodeString;
}
exports.utf8Decode = utf8Decode;
//# sourceMappingURL=helper.js.map