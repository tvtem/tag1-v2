"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = __importStar(require("../settings"));
function validateLogin(req) {
    return new Promise(function (resolve, reject) {
        try {
            var rd = req.body;
            if (!rd || !rd.email || !rd.password)
                reject('Invalid data received.');
            resolve({ email: rd.email, password: rd.password });
        }
        catch (err) {
            Settings.logError(err);
            reject('Client sent invalid POST information to server.');
        }
    });
}
exports.validateLogin = validateLogin;
function validateToken(req) {
    return new Promise(function (resolve, reject) {
        try {
            var rd = req.query;
            if (!rd || !rd.token)
                reject('Invalid data received.');
            resolve({ token: rd.token });
        }
        catch (err) {
            Settings.logError(err);
            reject('Client sent invalid POST information to server.');
        }
    });
}
exports.validateToken = validateToken;
function validateJSONData(req) {
    return new Promise(function (resolve, reject) {
        try {
            var rd = req.body;
            if (!rd)
                reject('Invalid data received.');
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err);
            reject('Client sent invalid POST information to server.');
        }
    });
}
exports.validateJSONData = validateJSONData;
function validateGetData(req) {
    return new Promise(function (resolve, reject) {
        try {
            var rd = req.query;
            if (!rd)
                reject('Invalid data received.');
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err);
            reject('Client sent invalid GET information to server.');
        }
    });
}
exports.validateGetData = validateGetData;
function validateReqParams(req) {
    return new Promise(function (resolve, reject) {
        try {
            var rd = req.params;
            if (!rd)
                reject('Invalid Req Param data received.');
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err);
            reject('Client sent invalid POST information to server.');
        }
    });
}
exports.validateReqParams = validateReqParams;
//# sourceMappingURL=validators.js.map