"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = __importStar(require("../settings"));
var mysql = require('mysql');
var RedisServer = require('ioredis');
var tokenRepository = __importStar(require("../database/token"));
exports.tokenRepository = tokenRepository;
var userRepository = __importStar(require("../database/user"));
exports.userRepository = userRepository;
var bannerRepository = __importStar(require("../database/banners"));
exports.bannerRepository = bannerRepository;
var redisRepository = __importStar(require("../database/redis"));
exports.redisRepository = redisRepository;
var Connection = null;
var Database = /** @class */ (function () {
    function Database() {
        this.Redis = null;
    }
    Database.prototype.create = function () {
        try {
            var _conn = mysql.createPool({
                connectionLimit: 200,
                host: '35.230.107.215',
                user: 'tvtem',
                password: 'tvtem@!2019',
                database: 'tag1'
            });
            Connection = _conn;
            Connection.getConnection(function (err, connection) {
                if (err) {
                    Settings.logError('Database Error: ' + err.message);
                    process.exit();
                }
                if (connection) {
                    Settings.logInfo('Database connected successfully.');
                }
            });
            this.Redis = new RedisServer();
            if (this.Redis.set('online', true)) {
                Settings.logInfo("Redis Server connected successfully (" + this.Redis.get('online') + ").");
            }
        }
        catch (err) {
            Settings.logError(err);
        }
        return this;
    };
    Database.prototype.destroy = function () {
    };
    Database.prototype.executeRaw = function (qry) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                            Connection.getConnection(function (err, connection) {
                                try {
                                    var result = connection.query(qry, function (err, rows, fields) {
                                        if (err) {
                                            Settings.logError(err.sqlMessage);
                                            connection.release();
                                            resolve({ error: true });
                                        }
                                        else {
                                            connection.release();
                                            resolve(JSON.parse(JSON.stringify(rows)));
                                        }
                                    });
                                }
                                catch (err) {
                                    reject(err);
                                }
                            });
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Database.prototype.getName = function (table, id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                            Connection.getConnection(function (err, connection) {
                                var qry = "SELECT `name` FROM `" + table + "` WHERE id='" + id + "' ";
                                var result = connection.query(qry, function (err, rows, fields) {
                                    if (err) {
                                        Settings.logError(err.sqlMessage);
                                        connection.release();
                                        resolve('NULL');
                                    }
                                    else {
                                        connection.release();
                                        var data = JSON.parse(JSON.stringify(rows))[0];
                                        if (data === undefined)
                                            resolve('');
                                        else {
                                            resolve(data.name);
                                        }
                                    }
                                });
                            });
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Database.prototype.getLastTableID = function (table) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                            Connection.getConnection(function (err, connection) {
                                var qry = "SELECT `id` FROM `" + table + "` ORDER BY `id` DESC LIMIT 1";
                                var result = connection.query(qry, function (err, rows, fields) {
                                    if (err) {
                                        Settings.logError(err.sqlMessage);
                                        connection.release();
                                        resolve(0);
                                    }
                                    else {
                                        connection.release();
                                        var data = JSON.parse(JSON.stringify(rows))[0];
                                        if (data === undefined)
                                            resolve(0);
                                        else {
                                            resolve(data.id);
                                        }
                                    }
                                });
                            });
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Database.prototype.filterQuery = function (string) {
        var newString = "" + string;
        newString = newString.replace(new RegExp("'", 'g'), "\\'");
        return newString;
    };
    Database.prototype.updateFields = function (table, fields, values, id) {
        return __awaiter(this, void 0, void 0, function () {
            var now, data, f, v, ii, i, commit, withData, qry, exec, withData, i, commit, sql;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        now = Date.now();
                        data = null;
                        fields.push('updated');
                        values.push(now);
                        if (!(id === "new")) return [3 /*break*/, 2];
                        fields.push('created');
                        values.push(now);
                        if (values.length < fields.length)
                            return [2 /*return*/, false];
                        f = '';
                        v = '';
                        ii = 0;
                        for (i = 0; i < fields.length; i++) {
                            commit = ii === 0 ? '' : ', ';
                            f = f + commit + '`' + fields[i] + '`';
                            v = v + commit + "'" + this.filterQuery(values[i]) + "'";
                            ii++;
                        }
                        withData = "(" + f + ") VALUES (" + v + ")";
                        qry = "INSERT INTO " + table + " " + withData;
                        return [4 /*yield*/, DB.executeRaw(qry)];
                    case 1:
                        exec = _a.sent();
                        data = exec.insertId;
                        return [3 /*break*/, 4];
                    case 2:
                        if (values.length < fields.length)
                            return [2 /*return*/, false];
                        withData = 'SET ';
                        for (i = 0; i < fields.length; i++) {
                            commit = i === 0 ? '' : ', ';
                            withData = withData + commit + ("`" + fields[i] + "`='") + this.filterQuery(values[i]) + "'";
                        }
                        sql = "UPDATE " + table + " " + withData + " WHERE `id`='" + id + "'";
                        return [4 /*yield*/, DB.executeRaw(sql)];
                    case 3:
                        data = _a.sent();
                        _a.label = 4;
                    case 4:
                        if (!data)
                            return [2 /*return*/, values && values.length > 0 ? values[0] : false];
                        return [2 /*return*/, data];
                }
            });
        });
    };
    return Database;
}());
var DB = new Database().create();
exports.DB = DB;
exports.default = Database;
//# sourceMappingURL=database.js.map