"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = __importStar(require("../settings"));
var database_1 = require("../services/database");
var Helper = __importStar(require("../helper"));
function getLatestBanners() {
    return __awaiter(this, void 0, void 0, function () {
        var query;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `banners` WHERE `archived`=0 ORDER BY `updated` DESC LIMIT 0,10 ")];
                case 1:
                    query = _a.sent();
                    return [2 /*return*/, {
                            items: query
                        }];
            }
        });
    });
}
exports.getLatestBanners = getLatestBanners;
function getBannersPreview(items) {
    return __awaiter(this, void 0, void 0, function () {
        var presel, previewId, insert;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `previews` WHERE `items`='" + items + "' ")];
                case 1:
                    presel = _a.sent();
                    if (presel.length > 0)
                        return [2 /*return*/, presel[0].key];
                    previewId = Helper.generateSafeUniqueID();
                    return [4 /*yield*/, database_1.DB.updateFields('previews', [
                            'key',
                            'items'
                        ], [
                            previewId,
                            items
                        ], 'new')];
                case 2:
                    insert = _a.sent();
                    if (insert && insert > 0)
                        return [2 /*return*/, previewId];
                    return [2 /*return*/, false];
            }
        });
    });
}
exports.getBannersPreview = getBannersPreview;
function getBannersPreviewData(key) {
    return __awaiter(this, void 0, void 0, function () {
        var presel, preview, items, pData, i, info, item, user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `previews` WHERE `key`='" + key + "' ")];
                case 1:
                    presel = _a.sent();
                    if (!(presel.length > 0)) return [3 /*break*/, 7];
                    preview = presel[0];
                    items = preview.items.split(',');
                    pData = [];
                    i = 0;
                    _a.label = 2;
                case 2:
                    if (!(i < items.length)) return [3 /*break*/, 6];
                    return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `banners` WHERE `id`='" + items[i] + "' ")];
                case 3:
                    info = _a.sent();
                    item = info[0];
                    return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `users` WHERE `id`='" + item.user + "'")];
                case 4:
                    user = _a.sent();
                    item.usuario = (user.length > 0) ? user[0].name : 'Unknown';
                    pData.push(item);
                    _a.label = 5;
                case 5:
                    i++;
                    return [3 /*break*/, 2];
                case 6: return [2 /*return*/, pData];
                case 7: return [2 /*return*/, {
                        error: 'Invalid Preview ID.'
                    }];
            }
        });
    });
}
exports.getBannersPreviewData = getBannersPreviewData;
function getBannerById(id) {
    return __awaiter(this, void 0, void 0, function () {
        var query;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.executeRaw("SELECT * FROM `banners` WHERE `id`='" + id + "' ")];
                case 1:
                    query = _a.sent();
                    if (query && !query.error)
                        return [2 /*return*/, query[0]];
                    return [2 /*return*/, false];
            }
        });
    });
}
exports.getBannerById = getBannerById;
function setClickURL(cid, url) {
    return __awaiter(this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.Redis.set("link:" + cid, url)];
                case 1:
                    result = _a.sent();
                    if (!result || result !== "OK")
                        return [2 /*return*/, false];
                    return [2 /*return*/, true];
            }
        });
    });
}
exports.setClickURL = setClickURL;
function getClickURL(cid) {
    return __awaiter(this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.Redis.get("link:" + cid)];
                case 1:
                    result = _a.sent();
                    Settings.logInfo("CID: " + cid + " -> " + result);
                    return [2 /*return*/, result];
            }
        });
    });
}
exports.getClickURL = getClickURL;
function registerBanner(banner) {
    return __awaiter(this, void 0, void 0, function () {
        var ID, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, database_1.DB.updateFields("banners", ["id", "user", "width", "height", "peso", "prints", "clicks", "platform"], [
                            banner.ID,
                            banner.User,
                            banner.Width,
                            banner.Height,
                            banner.Size,
                            0,
                            0,
                            banner.Platform
                        ], 'new')];
                case 1:
                    ID = _a.sent();
                    return [2 /*return*/, ID];
                case 2:
                    err_1 = _a.sent();
                    Settings.logError(err_1.message);
                    return [2 /*return*/, false];
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.registerBanner = registerBanner;
function archiveItems(items) {
    return __awaiter(this, void 0, void 0, function () {
        var itemArr, i, id, archive, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 5, , 6]);
                    itemArr = items.split(',');
                    i = 0;
                    _a.label = 1;
                case 1:
                    if (!(i < itemArr.length)) return [3 /*break*/, 4];
                    id = itemArr[i];
                    return [4 /*yield*/, database_1.DB.executeRaw("UPDATE `banners` SET `archived`=1 WHERE `id`='" + id + "' ")];
                case 2:
                    archive = _a.sent();
                    _a.label = 3;
                case 3:
                    i++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/, true];
                case 5:
                    err_2 = _a.sent();
                    Settings.logError(err_2.message);
                    return [2 /*return*/, false];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.archiveItems = archiveItems;
//# sourceMappingURL=banners.js.map