"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var database_1 = require("../services/database");
var Settings = __importStar(require("../settings"));
function get(section) {
    return __awaiter(this, void 0, void 0, function () {
        var value, log;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.Redis.get(section)];
                case 1:
                    value = _a.sent();
                    log = value ? Settings.logInfo("Cache found for " + section + ". Sending now!") : Settings.logInfo("Cache not found for " + section + ".");
                    return [2 /*return*/, value ? JSON.parse(value) : false];
            }
        });
    });
}
exports.get = get;
function set(value, section, expiration) {
    if (expiration === void 0) { expiration = 3600; }
    return __awaiter(this, void 0, void 0, function () {
        var set_1, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 3, , 4]);
                    return [4 /*yield*/, database_1.DB.Redis.set(section, JSON.stringify(value))];
                case 1:
                    set_1 = _a.sent();
                    Settings.logInfo("Cache set for " + section + ".");
                    return [4 /*yield*/, get(section)];
                case 2: return [2 /*return*/, _a.sent()];
                case 3:
                    err_1 = _a.sent();
                    Settings.logError("Redis Error: " + err_1.message);
                    return [2 /*return*/, false];
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.set = set;
function del(section) {
    return __awaiter(this, void 0, void 0, function () {
        var del;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.Redis.del(section)];
                case 1:
                    del = _a.sent();
                    Settings.logInfo("Cache deleted for " + section + ".");
                    return [2 /*return*/, true];
            }
        });
    });
}
exports.del = del;
function deleteWhere(pattern) {
    return __awaiter(this, void 0, void 0, function () {
        var stream, exec;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, database_1.DB.Redis.scanStream({
                        match: pattern
                    })];
                case 1:
                    stream = _a.sent();
                    return [4 /*yield*/, stream.on('data', function (resultKeys) {
                            return __awaiter(this, void 0, void 0, function () {
                                var i, section;
                                return __generator(this, function (_a) {
                                    // `resultKeys` is an array of strings representing key names.
                                    // Note that resultKeys may contain 0 keys, and that it will sometimes
                                    // contain duplicates due to SCAN's implementation in Redis.
                                    for (i = 0; i < resultKeys.length; i++) {
                                        section = resultKeys[i];
                                        del(section);
                                    }
                                    return [2 /*return*/];
                                });
                            });
                        })];
                case 2:
                    exec = _a.sent();
                    Settings.logInfo("Keys for " + pattern + " deleted successfully.");
                    return [2 /*return*/];
            }
        });
    });
}
exports.deleteWhere = deleteWhere;
//# sourceMappingURL=redis.js.map