"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var Settings = __importStar(require("./settings"));
var bodyParser = require('body-parser');
var cors = require('cors');
var database_1 = require("./services/database");
var login_1 = require("./routes/login");
var banners_1 = require("./routes/banners");
var FileUploader = __importStar(require("./routes/upload/file"));
function StartServer() {
    Settings.logInfo("INITIALIZING...");
    var app = express_1.default();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors());
    app.use('/static', express_1.default.static('static'));
    var sendNotFound = function (req, res) {
        res.status(404).send();
    };
    app.get('/', function (req, res) {
        res.send('Hello World!');
    });
    app.get('/login', sendNotFound);
    app.get('/token', database_1.tokenRepository.verifyToken, login_1.validateUserToken);
    app.post('/login', login_1.login);
    app.post('/token', sendNotFound);
    app.get('/banners/last', database_1.tokenRepository.verifyToken, banners_1.getLatest);
    app.post('/banners/preview', database_1.tokenRepository.verifyToken, banners_1.preparePreview);
    app.get('/banners/preview', banners_1.getPreview);
    app.get('/banners/embed/:id', banners_1.getAdScript);
    app.get('/banners/:link/:id.js', banners_1.getAdContent);
    app.get('/banners/:link/files/:id/:file', banners_1.getAdElement);
    app.post('/banners/archive', database_1.tokenRepository.verifyToken, banners_1.archiveItems);
    app.post('/uploadfiles', database_1.tokenRepository.verifyToken, FileUploader.BannerUpload, FileUploader.UploadBanners);
    app.post('/addinfo', database_1.tokenRepository.verifyToken, banners_1.updateInfo);
    app.get('/clickpreview', function (req, res) {
        res.status(200).send('Esta página garante que a url de clique está corretamente configurada.');
    });
    app.listen(8855, function () {
        Settings.logInfo("API STARTED...");
    });
}
exports.StartServer = StartServer;
//# sourceMappingURL=server.js.map