"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a = require('winston'), createLogger = _a.createLogger, format = _a.format, transports = _a.transports;
var combine = format.combine, timestamp = format.timestamp, label = format.label, printf = format.printf;
var files = new transports.File({ filename: 'api.log' });
var console = new transports.Console();
var myFormat = printf(function (info) {
    return info.timestamp + " [" + info.level + "]: " + info.message;
});
var LOGGER = createLogger({
    format: combine(timestamp(), myFormat),
    transports: [
        console,
        files
    ]
});
exports.logError = function (msg) {
    var stack = new Error().stack;
    LOGGER.error(msg + " - " + stack);
};
exports.logInfo = function (msg) {
    LOGGER.info("" + msg);
};
exports.API_SECRET = 'API_TEST_2018';
exports.API_BASE_URL = 'http://localhost:8855/';
//# sourceMappingURL=settings.js.map