import React from 'react'

export default class Prototypes {
    static Override() {

        // Numbers
        // eslint-disable-next-line
        Number.prototype.padLeft = function (n, str) {
            return Array(n - String(this).length + 1).join(str || '0') + this;
        }

        // eslint-disable-next-line
        Number.prototype.toMoney = function () {
            try {
                let num = this.toFixed(2).replace(`.`, `,`).toString()
                let snum = num.substr(0, num.length - 3)
                let lnum = num.length % 3
                let lnum1 = snum.substr(0, lnum)
                let lnum2 = snum.substr(lnum, snum.length - lnum)
                let pnum = lnum2.match(/.{3}/g).join('.')
                let rnum = (lnum1 + '.' + pnum)
                if (lnum === 0)
                    rnum = pnum
                let fnum = rnum + num.substr(num.length - 3, 3)

                //console.log({ num, snum, lnum, lnum1, lnum2, pnum, rnum, fnum })

                return `R$ ${fnum}`
            } catch (err) {
                return ""
            }
        }

        // Strings
        // eslint-disable-next-line
        String.prototype.isEmail = function () {
            // eslint-disable-next-line
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(this);
        }

        // eslint-disable-next-line
        String.prototype.replaceAll = function (find, replace) {
            var str = this;
            // eslint-disable-next-line
            return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
        };

        // Arrays
        // eslint-disable-next-line
        Array.prototype.clone = function () {
            return this.slice(0);
        };

        // React
        // Component
        React.Component.prototype.loadElement = function (element) {
            if (!this.state.loader) {
                let arr = []
                arr[element] = true
                this.setState({
                    loader: arr
                })
            } else {
                let loader = this.state.loader
                loader[element] = true

                this.setState({
                    loader: loader
                })
            }
        }

        React.Component.prototype.unloadElement = function (element) {
            if (!this.state.loader) {
                let arr = []
                arr[element] = false
                this.setState({
                    loader: arr
                })
            } else {
                let loader = this.state.loader
                loader[element] = false

                this.setState({
                    loader: loader
                })
            }
        }

        React.Component.prototype.loadingElement = function (element) {
            if (!this.state.loader)
                return false
            return this.state.loader[element] === true
        }
    }
}