import AuthService from './auth'
import StorageService from './storage'
import ApiService from './api'

class Services {

    constructor() {
        this.Auth = new AuthService()
        this.Storage = new StorageService()
        this.Api = new ApiService()
    }

}

let _services = new Services()

export default _services