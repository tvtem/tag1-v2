import React from 'react'
import axios from 'axios'
import { API_URL, USER_DATA, setUserData } from '../settings'

class ApiService extends React.Component {

    async get(url, data) {
        try {
            let instance = axios.create({
                headers: {
                    common: {
                    }
                }
            })
            if (USER_DATA && USER_DATA.token)
                instance.defaults.headers.common['x-access-token'] = USER_DATA.token

            const response = await instance.get(`${API_URL}/${url}`, {
                params: data
            })
            if (response && response.data && !response.data.auth) {
                if (response.data.expired) {
                    console.log('expired')
                    setUserData('LOGIN_EXPIRED', true)
                    window.location.href = '/login'
                }
            }

            return response;
        } catch (error) {
            console.error(error);
        }
    }

    async post(url, data) {
        try {
            let instance = axios.create({
                headers: {
                    common: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
            })
            if (USER_DATA && USER_DATA.token)
                instance.defaults.headers.common['x-access-token'] = USER_DATA.token

            const params = new URLSearchParams();
            Object.keys(data).forEach(function (key) {
                params.append(key, data[key])
            })
            const response = await instance.post(`${API_URL}/${url}`, params)
            return response;
        } catch (error) {
            return error;
        }
    }

    async postFile(url, data) {
        try {
            let instance = axios.create({
                headers: {
                    common: {
                    }
                }
            })

            if (USER_DATA && USER_DATA.token)
                instance.defaults.headers.common['x-access-token'] = USER_DATA.token

            const response = await instance.post(`${API_URL}/${url}`, data, {
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            return response;
        } catch (error) {
            return error;
        }
    }
}

export default ApiService