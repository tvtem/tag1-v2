import React from 'react';
import Services from '../services/services'
import * as Settings from '../settings'
import Noty from 'noty'

class AuthService extends React.Component {

    componentDidMount() {
    }

    sendInvalidLogin() {
        new Noty({
            title: 'Acesso Negado',
            text: 'Usuário ou Senha inválido(a).',
            layout: 'topCenter',
            timeout: '5000',
            type: 'error',
        }).show()
    }

    sendUnknownAlert() {
        new Noty({
            title: 'title',
            text: 'Erro desconhecido.',
            layout: 'topCenter',
            timeout: '5000',
            type: 'error',
        }).show()
    }

    async doLogin(data) {
        let req = await Services.Api.post('login', data)

        if ((req.response &&
            req.response.status === 401) || 
                !req.data.auth
            ) {
            // Login inválido
            this.sendInvalidLogin()
            return false;
        }

        return this.createSession(req.data)
    }

    createSession(data) {

        let session = data
        // add settings to session

        Services.Storage.set('USER_SESSION', JSON.stringify(session));

        return true
    }

    async doLogout(data) {
        //let req = await Services.Api.post('logout', data)

        //if (req.response &&
        //    req.response.status === 401) {
            // Login inválido

        //    return false;
        //}

        // Login válido
        // Expira o token
        //console.log(req.data)

        return this.destroySession()
    }

    destroySession() {
        Services.Storage.del('USER_SESSION');
        return true
    }

    async isLogged() {
        return await this.validLogin();
    }

    async validLogin() {
        let userData = Settings.USER_DATA
        if (userData && userData.token !== "") {

            let data = {
                token: userData.token,
            }

            let req = await Services.Api.get('token', data)

            try {
                return req.data.valid_token
            }
            catch (err) {
                return false;
            }
        }

        return false;
    }

    async getSessionData() {
        let userData = Settings.USER_DATA
        if (userData && userData.id && userData.role && userData.token && userData.token !== "") {

            let data = {
                token: userData.token,
            }

            return data

        }

        return {}
    }

    async unlockPage() {
        return await this.isLogged()
    }
}

export default AuthService;