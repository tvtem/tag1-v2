import React from 'react';

class StorageService extends React.Component {
    componentWillMount() {
    }

    set(key, value) {
        localStorage.setItem(key, value);
    }

    get(key) {
        return localStorage.getItem(key);
    }

    del(key) {
        return localStorage.removeItem(key);
    }
}

export default StorageService