import React, { Component } from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import LandingPage from './pages/landingPage'
import LoginPage from './pages/loginPage'
import MainPage from './pages/mainPage'
import PreviewPage from './pages/previewPage'
import './css/main.css'

export default class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={LandingPage} />
                    <Route exact path="/login" component={LoginPage} />.

                    <Route exact path="/app" component={MainPage} />

                    <Route exact path="/preview/:id" component={PreviewPage} />
                </Switch>
            </Router>
        )
    }
}