import React, { Component } from 'react'
import * as Settings from '../../settings'

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <figure>
                        <img src="/img/logo-light.png" alt="Logo" />
                    </figure>
                    <p>
                        Copyright &copy; {Settings.CURR_YEAR} TAG1
                    </p>
                    <p>
                        TV TEM
                    </p>
                </div>
            </footer>
        )
    }
}