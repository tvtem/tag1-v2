import React, { Component } from 'react'
import * as Settings from '../../settings'

export default class Nav extends Component {
    constructor(props) {
        super(props)

        let userData = Settings.USER_DATA

        this.state = {
            userData: userData,
            bgDark: props.bgdark ? props.bgdark : false
        }
    }

    render() {
        return (
            <nav className={`navbar navbar-expand-lg main-navbar ${this.state.bgDark ? 'bg-dark' : ''}`}>
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">
                        <img src="/img/logo-light.png" alt="Logo" />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon">
                            <i className="ion-navicon"></i>
                        </span>
                    </button>
                    {
                        this.state.userData && this.state.userData.token ? (
                            <ul className="navbar-nav">
                                <form className="form-inline">
                                    <div className="dropdown">
                                        <button className="btn smooth-link align-middle btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Olá, {this.state.userData.name}</button>
                                        <div className="dropdown-menu margin-top-10" aria-labelledby="dropdownMenuButton">
                                            <a className="dropdown-item" href="/app">Criativos</a>
                                            <a className="dropdown-item" href="/clients">Clientes</a>
                                        </div>
                                    </div>
                                </form>
                            </ul>
                        ) : (
                                <div className="collapse navbar-collapse" id="navbarNav">
                                    <div className="mr-auto"></div>

                                    <ul className="navbar-nav">
                                        <li className="nav-item">
                                            <a className="nav-link smooth-link" href="#hero">Home</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link smooth-link" href="#features">Sobre</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link smooth-link" href="#project">Projetos</a>
                                        </li>
                                    </ul>
                                    <form className="form-inline">
                                        <a href="/login" className="btn smooth-link align-middle btn-primary">Login</a>
                                    </form>
                                </div>
                            )
                    }
                </div>
            </nav>
        )
    }
}