import React, { Component } from 'react'
import { Tooltip, OverlayTrigger } from 'react-bootstrap'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import swal from 'sweetalert2'
import Services from '../../services/services'
import * as Helper from '../../helper'

export default class ItemList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            owner: props.owner ? props.owner : false,
            data: props.src ? props.src : {},
            copyValue: 'TESTE',
            copied: false,
            selectable: false,
            selectAll: false,
            selectItems: [],
        }
    }

    componentWillReceiveNewProps(props) {
        this.setState({
            owner: props.owner ? props.owner : false,
            data: props.src ? props.src : {},
        })
    }

    toggleSelectAll() {
        let select = false
        let items = []

        if (!this.state.selectAll) {
            select = true
        }

        if (select) {
            let elements = document.getElementsByClassName("selectItem")
            for (let i = 0; i < elements.length; i++) {
                let item = elements[i]
                items.push(item.id)
            }
        }

        this.setState({
            selectAll: select,
            selectItems: items
        })
    }

    toggleSingleSelect(element) {
        let items = this.state.selectItems

        if (!items.includes(element.id)) {
            items.push(element.id)
        } else {
            for (var i = 0; i < items.length; i++) {
                if (items[i] === element.id) {
                    items.splice(i, 1);
                }
            }
        }

        this.setState({
            selectItems: items
        })
    }

    toggleSelectable() {
        let selectable = false

        if (!this.state.selectable) {
            selectable = true
        }

        this.setState({
            selectable: selectable
        })
    }

    tooltip(html) {
        return (
            <Tooltip id="tooltip">
                {html}
            </Tooltip>
        )
    }

    confirmCopy(title, format) {
        swal({
            title: "Código copiado!",
            text: `[${format}] ${title}`,
            icon: 'success',
            buttons: false,
            timer: 3000,
        });
    }

    async generatePreviewURL() {
        let items = this.state.selectItems
        let pitems = []

        for (let i = 0; i < items.length; i++) {
            let item = items[i]
            pitems.push(item.replace("select", ""))
        }

        let req = await Services.Api.post('banners/preview', {
            items: pitems
        })

        if (req && req.data && req.data.preview) {
            window.location.href = `/preview/${req.data.preview}/`
        }
    }

    async previewOne(id) {
        if (id) {
            await this.setState({
                selectItems: [id]
            })

            this.generatePreviewURL()
        }
    }

    async archiveItems() {
        let items = this.state.selectItems
        let pitems = []

        for (let i = 0; i < items.length; i++) {
            let item = items[i]
            pitems.push(item.replace("select", ""))
        }

        let req = await Services.Api.post('banners/archive', {
            items: pitems
        })

        if (req && req.data && req.data.success) {
            await swal('Tudo certo!', 'O o(s) criativo(s) foi(ram) arquivado(s) com sucesso!', 'success')
            this.state.owner.getLastByDefault()
        } else {
            await swal('Ops!', `Não foi possível arquivar o(s) criativo(s).`, 'error')
        }
    }

    async prepareArchive() {
        let result = await swal({
            title: `Arquivar criativos?`,
            text: "Deseja realmente arquivar estes criativos?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Arquivar',
            cancelButtonText: 'Cancelar'
        })

        if (result.value) {
            this.archiveItems()
        }
    }

    async archiveOne(id, name) {
        if (id) {
            let result = await swal({
                title: `Arquivar "${name ? name : 'criativo sem descrição'}"?`,
                text: "Deseja realmente arquivar este criativo?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Arquivar',
                cancelButtonText: 'Cancelar'
            });

            if (result.value) {
                await this.setState({
                    selectItems: [id]
                })

                this.archiveItems()
            }
        }
    }

    getList() {
        let list = Object.values(this.state.data).map((item, i) => {
            let tag = Helper.TagStructure.replace('{id}', item.id)
            return (
                <tr key={i}>
                    <td className="selectCol">
                        <input type="checkbox" className="selectCheckBox selectItem" id={`select${item.id}`} name={`select${item.id}`} checked={this.state.selectItems.includes('select' + item.id)} onClick={(el) => { this.toggleSingleSelect(el.currentTarget) }} />
                        <label htmlFor={`select${item.id}`}></label>
                    </td>
                    <td>{i}</td>
                    <td>{Helper.getFormat(item.width, item.height)}</td>
                    <td>{item.descricao}</td>
                    <td>{Helper.getDate(item.updated)}</td>
                    <td>{Helper.getSize(item.peso)}</td>
                    <td>
                        <CopyToClipboard text={tag} onCopy={() => { this.confirmCopy(item.descricao, Helper.getFormat(item.width, item.height)) }}>
                            <OverlayTrigger placement="top" overlay={this.tooltip('Copiar Código')}>
                                <button className="btn btn-sm btn-primary margin-right-5"><i className="fa fa-code"></i></button>
                            </OverlayTrigger>
                        </CopyToClipboard>

                        <OverlayTrigger placement="top" overlay={this.tooltip('Previsualizar')}>
                            <button className="btn btn-sm btn-primary margin-right-5" onClick={() => { this.previewOne(`select${item.id}`) }}><i className="fa fa-search"></i></button>
                        </OverlayTrigger>

                        <OverlayTrigger placement="top" overlay={this.tooltip('Arquivar')}>
                            <button className="btn btn-sm btn-primary margin-right-5" onClick={() => { this.archiveOne(`select${item.id}`, item.descricao) }}><i className="fa fa-archive"></i></button>
                        </OverlayTrigger>
                    </td>
                </tr>
            )
        })

        return (
            <div>
                <div className="form-group text-right contact">
                    <button className={`btn darkbt btn-primary ${(this.state.selectable && this.state.selectItems.length > 0) ? 'btnShow' : 'btnToLeft'}`} onClick={this.prepareArchive.bind(this)}>Arquivar</button>
                    <button className={`btn darkbt btn-primary ${(this.state.selectable && this.state.selectItems.length > 0) ? 'btnShow' : 'btnToLeft'}`} onClick={this.generatePreviewURL.bind(this)}>Visualizar</button>
                    <button className="btn darkbt btn-primary" onClick={this.toggleSelectable.bind(this)}>Selecionar</button>
                </div>

                <table className={`table table-striped table-bordered table-fixed table-text-sm ${this.state.selectable ? 'selectable' : ''}`}>
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col" className="selectCol">
                                <input type="checkbox" className="selectCheckBox" id="selectAll" name="selectAll" checked={this.state.selectAll} onClick={this.toggleSelectAll.bind(this)} />
                                <label htmlFor="selectAll"></label>
                            </th>
                            <th scope="col" width="5%">#</th>
                            <th scope="col" width="11%">Tamanho</th>
                            <th scope="col" width="40%">Descrição</th>
                            <th scope="col">Data</th>
                            <th scope="col">Peso</th>
                            <th scope="col">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data && this.state.data.length ? list : (
                            <tr>
                                <td className="selectCol"></td>
                                <td colSpan={6}>
                                    Não há nenhum item para exibir.
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }

    render() {
        return (this.getList())
    }
}