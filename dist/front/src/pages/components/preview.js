import React, { Component } from 'react'
import * as Settings from '../../settings'
import * as Helper from '../../helper'
import moment from 'moment'
import { Tooltip, OverlayTrigger } from 'react-bootstrap'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import swal from 'sweetalert'

export default class Preview extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: props.data ? props.data : false,
        }
    }

    componentDidMount() {
        this.setState({
            rendered: true
        })
    }

    componentWillReceiveProps(props) {
        this.setState({
            data: props.data ? props.data : false,
        })
    }

    componentDidUpdate() {
        if (this.state.data && this.state.data.previewData && this.state.data.previewData.length > 0) {
            let items = this.state.data.previewData

            items.map((item, i) => {
                let script = document.createElement("script");
                script.src = `${Settings.API_URL}/banners/embed/${item.id}/?c=/clickpreview`;
                //script.async = true;
                let element = document.getElementById(`item${item.id}`)
                element.appendChild(script)
                return (<span key={i}></span>)
            })

        }
    }

    tooltip(html) {
        return (
            <Tooltip id="tooltip">
                {html}
            </Tooltip>
        )
    }

    confirmCopy(title, format) {
        swal({
            title: "Código copiado!",
            text: `[${format}] ${title}`,
            icon: 'success',
            buttons: false,
            timer: 3000,
        });
    }

    formatElement(i, key) {
        let format = Helper.getFormat(i.width, i.height)
        let update = moment.unix(i.updated / 1000)
        let tag = Helper.TagStructure.replace('{id}', i.id)

        return (
            <div key={key} className="row">
                <div className="col-xs-12 col-md-3">
                    <div className={`itemPreviewData item${format}`}>
                        {
                            Settings.USER_DATA && Settings.USER_DATA.token ? (
                                <CopyToClipboard text={tag} onCopy={() => { this.confirmCopy(i.descricao, Helper.getFormat(i.width, i.height)) }}>
                                    <OverlayTrigger placement="top" overlay={this.tooltip('Copiar Código')}>
                                        <button className="btn btn-sm btn-primary margin-left-5 floatElement"><i className="fa fa-code"></i></button>
                                    </OverlayTrigger>
                                </CopyToClipboard>
                            ) : (null)
                        }

                        <div>ID: {i.id}</div>
                        <div>Formato: {format} ({i.width}x{i.height}) - {i.platform}</div>
                        <div>Peso: {Helper.getSize(i.peso)}</div>
                        <div>Último Update: {update.format(`DD/MM/YYYY HH:mm:ss`)}</div>
                        <div>Update por: {i.usuario}</div>
                        <div>Descrição: {i.descricao}</div>
                    </div>
                </div>
                <div className="col-xs-12 col-md-9">
                    <div className={`itemPreview item${format}`} id={`item${i.id}`} key={key}>
                    </div>
                </div>
            </div>
        )

    }

    getElements() {
        if (this.state.data && this.state.data.previewData && this.state.data.previewData.length > 0) {
            let items = this.state.data.previewData

            let itemData = items.map((item, i) => {
                return this.formatElement(item, i)
            })

            return itemData
        }

        return (null)
    }

    render() {
        return (
            <div className="previewContainer">
                {this.getElements()}
            </div>
        )
    }
}