import React, { Component } from 'react'
import * as Settings from '../../settings'

export default class Footer extends Component {
    render() {
        return (
            <div className="footer footer-light">
                Copyright &copy; {Settings.CURR_YEAR} TAG1
            </div>
        )
    }
}