import React, { Component } from 'react'
import '../css/main.css'
import Footer from './components/footer'
import 'font-awesome/css/font-awesome.min.css'
import Nav from './components/nav.js'
import Services from '../services/services'
import ItemList from './components/itemList'
import Dropzone from 'react-dropzone'
import swal from 'sweetalert2'
import * as Settings from '../settings'

export default class MainPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loader: [],
            selectable: false,
            uploads: [],
        }
    }

    async componentDidMount() {
        this.getLastByDefault()
    }

    doSearch(e) {
        if (!this.loadingElement('listData')) {
            this.loadElement('listData')
        }

        e.preventDefault();
        return false;
    }

    async getLastByDefault() {
        if (!this.loadingElement('listData')) {
            this.loadElement('listData')

            let req = await Services.Api.get('banners/last')

            if (req && req.data && req.data.items) {
                this.setState({
                    listData: req.data.items
                })
            }

            this.unloadElement('listData')
        }
    }

    printScript(id) {
        let script = document.createElement("script");
        script.src = `${Settings.API_URL}/banners/embed/${id}/?c=/clickpreview`;
        //script.async = true;
        let item = document.createElement("div")
        item.id = `item${id}`
        item.appendChild(script)

        document.body.appendChild(item)

        let itemn = document.getElementById(`item${id}`)
        let html = itemn.outerHTML

        itemn.parentNode.removeChild(itemn)
        return html

        //let element = document.getElementById(`pv${id}`)
        //element.appendChild(script)
    }



    async onDrop(files) {
        if (this.loadingElement('upload'))
            return false;

        this.loadElement('upload')
        this.setState({
            uploads: files
        });

        for (let i = 0; i < files.length; i++) {
            let formData = new FormData();
            let file = files[i]
            formData.append("file", file);

            let response = await Services.Api.postFile('uploadfiles', formData)

            let html = this.printScript(response.data.id)
            console.log(html)

            if (response.data)
                if (!response.data.error) {
                    let style = `<style>h2.swal2-title {display: block !important;}.swal2-popup{width: 64em !important;}</style>`
                    await swal({
                        title: `Adicione uma descrição a "${file.name}": <div class="itemPreviewBox" id="pv${response.data.id}">${html}</div>${style}`,
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Salvar',
                        cancelButtonText: 'Cancelar',
                        showLoaderOnConfirm: true,
                        preConfirm: async (desc) => {
                            let res = await
                                Services.Api.post('addinfo', {
                                    description: desc,
                                    id: response.data.id
                                })

                            console.log(res.data)
                            if (!res || !res.data) {
                                swal.showValidationMessage(
                                    `Upload falhou: Não foi possível atualizar as informações.`
                                )
                            }
                            else if (res.data.error) {
                                swal.showValidationMessage(
                                    `Upload falhou: ${res.data.error}`
                                )
                            } else {
                                await swal('Tudo certo!', 'O arquivo foi salvo com sucesso!', 'success')
                            }

                        },
                        allowOutsideClick: false,
                    }).then(async (result) => {
                    })
                } else {
                    if (response.data.error === 'INVALID_FILE')
                        await swal('Ops!', `O arquivo "${file.name}" não está no formato correto!`, 'error')
                }
        }

        this.unloadElement('upload')
        this.getLastByDefault()
    }

    getDropZone() {
        return (
            <section className="dropzoneBox">
                <div className="dropzone">
                    <Dropzone onDrop={this.onDrop.bind(this)} onDragEnter={(el) => { el.currentTarget.classList.add('dragHover') }} onDragLeave={(el) => { el.currentTarget.classList.remove('dragHover') }}>
                        <p>{this.loadingElement('upload') ? (<i className="fa fa-spinner fa-spin"></i>) : `Arraste os arquivos ou clique aqui.`}</p>
                    </Dropzone>
                </div>
            </section>
        );
    }

    render() {
        return (
            <div className="page">
                <Nav bgdark />
                <section className="rollNav margin-bottom-20">
                    <div className="mainCard">
                        <h3>Criativos</h3>
                        <hr />

                        <form className="contact" onSubmit={(e) => { this.doSearch(e) }}>
                            <div className="row">
                                <div className="col-2">
                                    <button className="btn btn-block btn-primary" onClick={(e) => { this.doSearch(e) }}>Buscar</button>
                                </div>
                                <div className="col-10">
                                    <input type="text" className="form-control" placeholder="Pesquisar Cliente / Banner / Campanha" id="searchFilter" name="searchFilter" required="" />
                                </div>
                            </div>
                        </form>

                        {this.getDropZone()}

                        <div className="searchResult" id="searchResult">{
                            this.state.loader.listData ? (<div className="searchLoader"><i className="fa fa-spinner fa-spin"></i></div>) : (
                                <ItemList src={this.state.listData} selectable={this.state.selectable} owner={this} />
                            )
                        }</div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}