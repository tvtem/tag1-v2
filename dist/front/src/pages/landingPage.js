import React, { Component } from 'react'
import '../css/main.css'
import Nav from './components/nav'
import Footer from './components/footer'

export default class LandingPage extends Component {
    render() {
        return (
            <div className="page">
                <Nav />
                <section className="hero bg-overlay" id="hero" data-bg="img/hero.jpeg">
                    <div className="text py-5">
                        <p className="lead">Sistema de controle e gerenciamento de publicidades</p>
                        <h1>O <span className="bold">amigo</span> dos Operadores <span className="bold">Comerciais</span>.</h1>
                        <div className="cta">
                            <a href="#features" className="btn btn-primary smooth-link">Conheça</a>
                        </div>
                    </div>
                </section>

                <section className="padding" id="features">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-sm-12">
                                <div className="list-item">
                                    <div className="icon">
                                        <i className="ion-merge"></i>
                                    </div>
                                    <div className="desc">
                                        <h2>Integração Operacional</h2>
                                        <p>
                                            Construído para permitir a comunicação entre os departamentos, facilitando o fluxo de trabalho e reduzindo falhas operacionais.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 col-sm-12">
                                <div className="list-item">
                                    <div className="icon">
                                        <i className="ion-code"></i>
                                    </div>
                                    <div className="desc">
                                        <h2>Estrutura Flexível</h2>
                                        <p>
                                            Desenvolvido com a tecnologia React, o sistema tem usabilidade agradável e facilmente adaptável às mudanças de tecnologias de terceiros.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 col-sm-12">
                                <div className="list-item no-spacing">
                                    <div className="icon">
                                        <i className="ion-android-lock"></i>
                                    </div>
                                    <div className="desc">
                                        <h2>Seguro</h2>
                                        <p>
                                            Submetido a diversos cenários intensos de resistência, o sistema contou com atualizações de segurança constantes e boas práticas de segurança.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="bg-overlay padding" id="project" data-bg="img/projects.jpeg">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-12 col-md-6">
                                <figure className="projects-picture">
                                    <img src="img/youzhang.png" alt="Youzhang" />
                                </figure>
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="projects-details">
                                    <div className="projects-badge">
                                        Publicidade
                                    </div>
                                    <h2 className="projects-title">Projetos Criativos</h2>
                                    <p className="projects-description">
                                        Com suporte para diversos tipos de publicidades e suas tecnologias, com o TAG1 é possível realizar campanhas publicitárias mais interativas e criativas, e o melhor: com uma grande quantidade de dados de análise de performance.
                                    </p>
                                    <div className="projects-cta">
                                        <a href="/app" className="btn btn-primary">
                                            Começar Agora
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="callout">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-12 col-md-8 text">
                                <h3>Melhore o fluxo da operação. Comece agora mesmo!</h3>
                            </div>
                            <div className="col-12 col-md-4 cta">
                                <a href="/app" className="btn btn-outline-primary">
                                    Começar
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <Footer />
            </div>


        )
    }
}