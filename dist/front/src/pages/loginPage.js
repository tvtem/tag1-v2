import React, { Component } from 'react'
import '../css/main.css'
import Footer from './components/footer-single'
import 'font-awesome/css/font-awesome.min.css'
import Services from '../services/services'
import * as Settings from '../settings'
import Noty from 'noty'

export default class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loader: [],

        }
    }

    loadElement(element) {
        let loader = this.state.loader
        loader[element] = true
        this.setState({
            loader: loader
        })
    }

    unloadElement(element) {
        let loader = this.state.loader
        loader[element] = false
        this.setState({
            loader: loader
        })
    }

    sendExpiredLogin() {
        new Noty({
            title: 'Acesso Negado',
            text: 'O seu acesso expirou. Faça login novamente.',
            layout: 'topCenter',
            timeout: '5000',
            type: 'error',
        }).show()
    }

    componentDidMount() {
        if (Settings.USER_DATA) {
            const { LOGIN_EXPIRED } = Settings.USER_DATA;
            if (LOGIN_EXPIRED) {
                this.sendExpiredLogin()
                Settings.unsetUserData('LOGIN_EXPIRED')
            }
        }
    }

    async sendData(e) {
        if (!this.state.loader.lbtn) {
            this.loadElement('lbtn')

            let email = document.getElementById('email').value
            let password = document.getElementById('password').value

            let response = await Services.Auth.doLogin({ email, password })

            if (response) {
                window.location.href = '/app'
            }

            this.unloadElement('lbtn')
        }

        e.preventDefault();
        return false;
    }

    render() {
        return (
            <div className="page">
                <section className="hero bg-overlay bg-overlay-soft" id="hero" data-bg="img/loginbg.jpeg">
                    <div className="text py-5 text-center">
                        <div className="card-wrapper">
                            <div className="brand">
                                <img className="loginLogo" src="img/logo-light.png" alt="" />
                            </div>
                            <div className="card midcard fat">
                                <div className="card-body">
                                    <h4 className="card-title">Login</h4>

                                    <form onSubmit={(e) => { this.sendData(e) }}>
                                        <div className="form-group">
                                            <label htmlFor="email">Email</label>
                                            <input id="email" type="email" className="form-control" name="email" required="" />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="password">Senha</label>
                                            <a href="/forgot-password" className="float-right">
                                                Esqueceu a senha?
                                            </a>
                                            <input id="password" type="password" className="form-control" name="password" required="" />
                                        </div>

                                        <div className="form-group no-margin">
                                            <p>&nbsp;</p>
                                        </div>

                                        <div className="form-group no-margin">
                                            <button className="btn btn-primary btn-block" onClick={(e) => { this.sendData(e) }}>
                                                {this.state.loader.lbtn ? (<i className="fa fa-spinner fa-spin"></i>) : 'Login'}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <Footer />
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}