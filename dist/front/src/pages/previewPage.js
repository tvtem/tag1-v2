import React, { Component } from 'react'
import '../css/main.css'
import Footer from './components/footer'
import 'font-awesome/css/font-awesome.min.css'
import Nav from './components/nav.js'
import Services from '../services/services'
import Preview from './components/preview'

export default class PreviewPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loader: [],
            id: props.match.params.id,
            previewData: false,
        }
    }

    loadElement(element) {
        let loader = this.state.loader
        loader[element] = true
        this.setState({
            loader: loader
        })
    }

    unloadElement(element) {
        let loader = this.state.loader
        loader[element] = false
        this.setState({
            loader: loader
        })
    }

    async componentDidMount() {
        this.loadData()
    }

    async loadData() {
        this.loadElement('previewData')

        let req = await Services.Api.get(`banners/preview?key=${this.state.id}`)

        if (req && req.data) {
            this.setState({
                previewData: req.data
            })
        }

        this.unloadElement('previewData')
    }

    render() {
        return (
            <div className="page">
                <Nav bgdark />
                <section className="rollNav margin-bottom-20">
                    <div className="mainCard">
                        <div className="pull-right form-group text-right contact">
                            <button className="btn btn-block darkbt btn-sm btn-primary" onClick={() => {
                                window.history.back();
                            }}>Voltar</button>
                        </div>
                        <h3>Preview</h3>
                        <hr />
                        <div className="previewResult" id="previewResult">{
                            this.state.loader.previewData ? (<div className="searchLoader"><i className="fa fa-spinner fa-spin"></i></div>) : (
                                <Preview data={this.state.previewData} />
                            )
                        }</div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}