import * as Settings from './settings'
const moment = require('moment')

export function getFormat(w, h) {

    let size = `${w}x${h}`

    switch (size) {
        case '300x250': return 'RMD';
        case '970x150': return 'Maxiboard';
        case '970x250': return 'Billboard';
        case '300x50': return 'Slim';
        default: return size;
    }
}

export function getSize(size) {
    size = Number.parseInt(size, 10)
    let times = 0

    while (size > 1024 && times < 6) {
        size = size / 1024
        times++
    }

    switch (times) {
        default:
        case 0: return `${size.toFixed(2)} bytes`;
        case 1: return `${size.toFixed(2)} kb`;
        case 2: return `${size.toFixed(2)} mb`;
        case 3: return `${size.toFixed(2)} gb`;
        case 4: return `${size.toFixed(2)} tb`;
        case 5: return `${size.toFixed(2)} pb`;
    }
}

export function getDate(timestamp) {
    let date = moment.unix(timestamp / 1000)
    return date.format("DD/MM/YYYY hh:mm:ss")
}

export const TagStructure = `<script src="${Settings.API_URL}/banners/embed/{id}/?c=%%CLICK_URL_ESC%%%%DEST_URL%%"></script>`