import React from 'react'
import Services from './services/services'
import moment from 'moment'
import Prototypes from './prototypes'

export const CURR_YEAR = moment.unix(Date.now() / 1000).format('YYYY')

//  Project Info    
export const APP_NAME = 'TAG1';

// API End-point
// Localhost For DEV only
export const API_URL = 'http://localhost:8855';

// IMG Service
// Localhost For DEV only
//export const IMG_URL = 'http://localhost:3333/img/';

// FILE Service
// Localhost For DEV only
//export const IMG_URL = 'http://localhost:3333/file/';


// Gets current session info for user
export const setUserData = (key, value) => {
    try {
        let s = Services.Storage.get('USER_SESSION')
        if (s) {
            let data = JSON.parse(s);
            data[key] = value;
            Services.Storage.set('USER_SESSION', JSON.stringify(data))
        }

    } catch (err) {
        return {}
    }
}
export const unsetUserData = (key) => {
    return setUserData(key, undefined)
}
const getUserData = () => {
    try {
        let s = Services.Storage.get('USER_SESSION')
        if (s) {
            return JSON.parse(s);
        }

    } catch (err) {
        return {}
    }
}
export const USER_DATA = getUserData()

// DEV MODE?
// This affects elements on screen that may show only for developers. Turning it into false, will remove any element for dev only.
// WARNING! DO NOT use this enabled in production.
const DEV_MODE = true;

export class DevModeOnly extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            children: props.children,
        }
    }

    render() {
        if (DEV_MODE) {
            return (<span className="devMode">{this.props.children}</span>)
        }
        else
            return null
    }
}

Prototypes.Override()