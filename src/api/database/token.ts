var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
import * as Settings from '../settings'
import { validateJSONData, validateReqParams, validateGetData } from '../services/validators';

export function generateToken(id: any) {
    // create a token
    var token = jwt.sign({ userId: id }, Settings.API_SECRET, {
        expiresIn: 86400 // expires in 24 hours
    });

    return token;
}

export function verifyToken(req: any, res: any, next: any) {
    var token = req.headers['x-access-token'];
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, Settings.API_SECRET, async function (err: any, decoded: any) {
        if (err) {
            if (err.message === 'jwt expired')
                return res.status(203).send({ auth: false, expired: true });
            return res.status(203).send({ auth: false, message: 'Failed to authenticate token.' });
        }
        
        req.userId = decoded.userId;
        req.postData = await validateJSONData(req)
        req.getData = await validateGetData(req)
        req.paramData = await validateReqParams(req)
        next();
    });
}