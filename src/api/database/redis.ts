import { DB } from '../services/database'
import * as Settings from '../settings'
import { AnyMxRecord } from 'dns';

export async function get(section: string) {
    let value: any = await DB.Redis.get(section)

    let log = value ? Settings.logInfo(`Cache found for ${section}. Sending now!`) : Settings.logInfo(`Cache not found for ${section}.`)

    return value ? JSON.parse(value) : false
}

export async function set(value: any, section: string, expiration: number = 3600) {
    try {
        let set = await DB.Redis.set(section, JSON.stringify(value))

        Settings.logInfo(`Cache set for ${section}.`)

        return await get(section)
    }
    catch (err) {
        Settings.logError(`Redis Error: ${err.message}`)
        return false
    }
}

export async function del(section: any) {
    let del = await DB.Redis.del(section)

    Settings.logInfo(`Cache deleted for ${section}.`)

    return true
}

export async function deleteWhere(pattern: string) {
    let stream = await DB.Redis.scanStream({
        match: pattern
    });
    let exec = await stream.on('data', async function (resultKeys: any) {
        // `resultKeys` is an array of strings representing key names.
        // Note that resultKeys may contain 0 keys, and that it will sometimes
        // contain duplicates due to SCAN's implementation in Redis.
        for (var i = 0; i < resultKeys.length; i++) {
            let section = resultKeys[i]
            del(section)
        }
    });
    Settings.logInfo(`Keys for ${pattern} deleted successfully.`)
}