import * as Settings from '../settings'
import { DB, bannerRepository } from '../services/database'
import * as Helper from '../helper'
import { FileObject } from '../routes/upload/handlers';

export async function getLatestBanners() {
    let query = await DB.executeRaw(`SELECT * FROM \`banners\` WHERE \`archived\`=0 ORDER BY \`updated\` DESC LIMIT 0,10 `)
    return {
        items: query
    }
}

export async function getBannersPreview(items: string) {
    let presel: any = await DB.executeRaw(`SELECT * FROM \`previews\` WHERE \`items\`='${items}' `)
    if (presel.length > 0)
        return presel[0].key

    let previewId = Helper.generateSafeUniqueID()
    let insert = await DB.updateFields('previews', [
        'key',
        'items'
    ], [
            previewId,
            items
        ],
        'new')
    if (insert && insert > 0)
        return previewId
    return false
}

export async function getBannersPreviewData(key: string) {
    let presel: any = await DB.executeRaw(`SELECT * FROM \`previews\` WHERE \`key\`='${key}' `)
    if (presel.length > 0) {
        let preview = presel[0]
        let items = preview.items.split(',')
        let pData: any = []

        for (let i = 0; i < items.length; i++) {
            let info: any = await DB.executeRaw(`SELECT * FROM \`banners\` WHERE \`id\`='${items[i]}' `)
            let item: any = info[0]
            let user: any = await DB.executeRaw(`SELECT * FROM \`users\` WHERE \`id\`='${item.user}'`)
            item.usuario = (user.length > 0) ? user[0].name : 'Unknown'
            pData.push(item)
        }

        return pData

    } else {
        return {
            error: 'Invalid Preview ID.'
        }
    }
}

export async function getBannerById(id: number) {
    let query: any = await DB.executeRaw(`SELECT * FROM \`banners\` WHERE \`id\`='${id}' `)
    if (query && !query.error)
        return query[0]
    return false
}

export async function setClickURL(cid: any, url: any) {
    let result: any = await DB.Redis.set(`link:${cid}`, url)
    if (!result || result !== "OK")
        return false
    return true
}

export async function getClickURL(cid: any) {
    let result: any = await DB.Redis.get(`link:${cid}`)
    Settings.logInfo(`CID: ${cid} -> ${result}`)
    return result
}

export async function registerBanner(banner: FileObject) {
    try {
        let ID = await DB.updateFields(`banners`, [`id`, `user`, `width`, `height`, `peso`, `prints`, `clicks`, `platform`], [
            banner.ID,
            banner.User,
            banner.Width,
            banner.Height,
            banner.Size,
            0,
            0,
            banner.Platform
        ], 'new')

        return ID
    } catch (err) {
        Settings.logError(err.message)
        return false
    }
}

export async function archiveItems(items: string) {
    try {
        let itemArr = items.split(',')
        for (let i = 0; i < itemArr.length; i++) {
            let id = itemArr[i]
            let archive = await DB.executeRaw(`UPDATE \`banners\` SET \`archived\`=1 WHERE \`id\`='${id}' `)
        }
        return true
    } catch (err) {
        Settings.logError(err.message)
        return false
    }
}