import express from 'express'
import * as Settings from './settings';
const bodyParser = require('body-parser');
const cors = require('cors');

import { tokenRepository } from './services/database'

import { login, validateUserToken } from './routes/login'
import { getLatest, getPreview, preparePreview, getAdScript, getAdContent, getAdElement, updateInfo, archiveItems } from './routes/banners'
import * as FileUploader from './routes/upload/file';

export function StartServer() {

    Settings.logInfo(`INITIALIZING...`);

    var app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors());
    app.use('/static', express.static('static'));

    let sendNotFound = (req: any, res: any) => {
        res.status(404).send()
    }

    app.get('/', function (req, res) {
        res.send('Hello World!');
    });

    app.get('/login', sendNotFound)
    app.get('/token', tokenRepository.verifyToken, validateUserToken)

    app.post('/login', login)
    app.post('/token', sendNotFound)

    app.get('/banners/last', tokenRepository.verifyToken, getLatest)
    app.post('/banners/preview', tokenRepository.verifyToken, preparePreview)
    app.get('/banners/preview', getPreview)
    app.get('/banners/embed/:id', getAdScript)
    app.get('/banners/:link/:id.js', getAdContent)
    app.get('/banners/:link/files/:id/:file', getAdElement)
    app.post('/banners/archive', tokenRepository.verifyToken, archiveItems)


    app.post('/uploadfiles', tokenRepository.verifyToken, FileUploader.BannerUpload, FileUploader.UploadBanners)
    app.post('/addinfo', tokenRepository.verifyToken, updateInfo)

    app.get('/clickpreview', (req: any, res: any) => {
        res.status(200).send('Esta página garante que a url de clique está corretamente configurada.')
    })


    app.listen(8855, function () {
        Settings.logInfo(`API STARTED...`);
    });
}