const fs = require("fs")
import Handlers from './handlers/'

export class FileObject {
    ID: string = 'unknown'
    Identified: boolean = false
    Data: any = null
    Width: number = 0
    Height: number = 0
    Platform: string = 'None'
    User: number = 0
    Size: number = 0
}

async function StartHandling(file: FileObject) {
    await Handlers.forEach(async (h: any) => {
        file = await h(file)
    });

    return file
}

export async function HandleAdFile(filename: string, id: string, size: number, user: number) {
    let file = fs.readFileSync(filename, "utf8")

    let fileObj = new FileObject()
    fileObj.Data = file
    fileObj.ID = id
    fileObj.Size = size
    fileObj.User = user

    return await StartHandling(fileObj)
}