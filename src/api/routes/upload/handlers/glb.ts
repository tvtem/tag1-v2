export default async function GLB(file: any) {
    if (!file.Identified) {
        let content = file.Data.split(`bannerSize = [`);
        if (content.length >= 2) {
            content = content[1];
            content = content.split("]");
            content = content[0].split(",");

            let w = content[0];
            let h = content[1];

            file.Width = w
            file.Height = h
            file.Identified = true
            file.Platform = "GLB"
        }
    }

    return file
}