export default async function GWD(file: any) {
    if (!file.Identified) {
        let content = file.Data.split(`<div is=\"gwd-page\"`);
        if (content.length < 2)
            content = file.Data.split(`<gwd-page id=`);
        if (content.length >= 2) {
            content = content[1];
            content = content.split("px\" data-gwd-height=\"");

            let w = content[0];
            w = w.split("data-gwd-width=\"");
            w = w[1];


            let h = content[1];
            h = h.split("px\"");
            h = h[0];

            file.Width = w
            file.Height = h
            file.Identified = true
            file.Platform = "GWD"
        }
    }

    return file
}