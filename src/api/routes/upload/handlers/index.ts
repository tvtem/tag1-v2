import GWD from './gwd'
import GLB from './glb'
import ADA from './ada'

let Handlers = [GWD, GLB, ADA]

export default Handlers