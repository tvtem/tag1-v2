export default async function ADA(file: any) {
    if (!file.Identified) {
        let content = file.Data.split(`Adobe_Animate_CC`);
        if (content.length >= 2) {
            content = content[1];
            content = content.split(`id="canvas" width="`);
            content = content[1].split(`"`);

            let w = content[0];
            let h = content[2];

            file.Width = w
            file.Height = h
            file.Identified = true
            file.Platform = "ADA"
        }
    }

    return file
}