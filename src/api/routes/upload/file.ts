import * as Settings from '../../settings';
import { HandleAdFile } from './handlers';
import { bannerRepository, redisRepository } from '../../services/database';

const multer = require('multer');
const crypto = require("crypto");
const JSZip = require("jszip");
const fs = require("fs");

function isValidExt(ext: string) {
    switch (ext) {
        case "rar":
        case "zip": return true

        default: return false
    }
}

var storage = multer.diskStorage({
    destination: (req: any, file: any, callback: any) => {
        callback(null, './uploads/banners');
    },
    filename: (req: any, file: any, callback: any) => {

        const id = Date.now() + '' + crypto.randomBytes(8).toString("hex");
        const type = file.originalname.split('.');
        const ext = type[type.length - 1];

        file.type = ext

        if (!isValidExt(ext)) {
            file.error = `INVALID_FILE`
        }

        file.id = id + '.' + ext
        file.bannerId = id
        callback(null, file.id);
    }
});

let BannerUpload = multer({ storage: storage }).single('file');

export { BannerUpload }

async function ExtractZip(zip: any, zipf: any, dir: string) {
    let file = 'index.html'
    for (let i = 0; i < Object.keys(zipf.files).length; i++) {
        let filename = Object.keys(zipf.files)[i]
        await zip.file(filename).async('nodebuffer').then(function (content: any) {
            let dest = `${dir}${filename}`
            if (!fs.existsSync(dir)) fs.mkdirSync(dir);
            fs.writeFileSync(dest, content);
        });

        let regex = new RegExp(/(.*).html/)
        if (regex.test(filename)) {
            file = filename
            continue;
        }
    }
    return `${dir}${file}`
}

export async function UploadBanners(req: any, res: any, err: any) {
    let zip = new JSZip();
    let fileObj: any = null
    try {
        // read a zip file
        fs.readFile(`uploads/banners/${req.file.id}`, async function (err: any, data: any) {
            if (err || req.file.error) {
                res.json({ success: false, error: req.file.error })
            } else {
                await zip.loadAsync(data).then(async function (zipf: any) {
                    let dir = `files/${req.file.bannerId}/`
                    //let index = `${dir}index.html`

                    let index = await ExtractZip(zip, zipf, dir)

                    if (!fs.existsSync(index))
                        req.file.error = 'INVALID_FILE'
                    else {
                        fileObj = await HandleAdFile(index, req.file.bannerId, req.file.size, req.userId)

                        let insert = await bannerRepository.registerBanner(fileObj)
                        let redis = await redisRepository.deleteWhere('banners:*')
                        if (!insert) req.file.error = 'DATABASE_ERROR'
                    }

                    res.json({ success: req.file.error ? false : true, id: req.file.bannerId, error: req.file.error ? req.file.error : null })
                });
            }
        });

    } catch (err) {
        Settings.logError(err.message)
    }
}