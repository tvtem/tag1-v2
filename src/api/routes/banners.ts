import * as Settings from '../settings';
import { validateJSONData, validateReqParams, validateGetData } from '../services/validators';
import { NextFunction, Request, Response } from 'express';
import { bannerRepository, redisRepository, DB } from '../services/database';
import * as Helper from '../helper'
import { fstat } from 'fs';
var legacy = require('legacy-encoding');
var fs = require('fs');

export async function getLatest(req: Request, res: Response): Promise<void> {
    let data: any = null
    let cached: any = await redisRepository.get('banners:latest')
    cached ? data = cached : data = await redisRepository.set(await bannerRepository.getLatestBanners(), 'banners:latest')
    res.status(200).json(data)
}

export async function getPreview(req: Request, res: Response): Promise<void> {
    req.getData = await validateGetData(req)
    let previewData: any = null
    let cached: any = await redisRepository.get(`banners:preview:${req.getData.key}`)
    cached ? previewData = cached : await redisRepository.set(previewData = await bannerRepository.getBannersPreviewData(req.getData.key), `banners:preview:${req.getData.key}`)
    res.status(200).json({ previewData: previewData })
}

export async function preparePreview(req: Request, res: Response): Promise<void> {
    if (!req.postData.items) {
        Settings.logError('Invalid item data for banners preview generation.')
        res.status(400).send()
    } else {
        let previewId = await bannerRepository.getBannersPreview(req.postData.items)
        res.status(200).json({ preview: previewId })
    }
}

export async function getAdScript(req: Request, res: Response): Promise<void> {
    req.paramData = await validateReqParams(req)
    req.getData = await validateGetData(req)

    if (req.paramData.id) {
        let cachedItem = await redisRepository.get(`banners:item:${req.paramData.id}`)

        let item: any = cachedItem ? cachedItem : await redisRepository.set(await bannerRepository.getBannerById(req.paramData.id), `banners:item:${req.paramData.id}`)
        let cid: any = Helper.generateSaferUniqueID()
        let frameId = `tag1_ad_${cid}`
        let script = `  var currpage = document.referrer;            
                        console.log('Referrer: ' + currpage);

                        var css = '#${frameId} { border: none; overflow: hidden; width: ${item.width}px; height: ${item.height}px; }';
                        var style = document.createElement('style');
                        style.type = 'text/css';
                        if (style.styleSheet){
                            // This is required for IE8 and below.
                            style.styleSheet.cssText = css;
                        } else {
                            style.appendChild(document.createTextNode(css));
                        }

                        var frame = document.createElement('iframe');
                        frame.id = '${frameId}';
                        frame.srcdoc = '<!DOCTYPE html><html><head><title>TAG1</title><base href="${Settings.API_BASE_URL}banners/${cid}/files/${req.paramData.id}/"><style>body { margin: 0; padding: 0; }</style></head><body><script src="${Settings.API_BASE_URL}banners/${cid}/${req.paramData.id}.js?ref='+currpage+'"></script></body></html>';
                    
                        document.body.appendChild(style);
                        document.getElementById('item${req.paramData.id}').appendChild(frame);`;


        if (await bannerRepository.setClickURL(cid, req.getData.c)) {
            res.status(200).type('.js').send(script)

        } else {
            Settings.logError('Failed to Register to Redis.')
            res.status(400).send()
        }
    } else {
        Settings.logError('Invalid item id for banner script.')
        res.status(400).send()
    }
}

export async function getAdContent(req: Request, res: Response): Promise<void> {
    let data = await validateReqParams(req)
    let json = await validateJSONData(req)

    if (data.id && data.link) {
        Settings.logInfo(`Printing Banner: ${data.id} with CID: ${data.link}`)
        let url = await bannerRepository.getClickURL(data.link)

        let content = await Helper.readFile(await getClosestHTML(`./files/${data.id}/`)).catch(err => {
            Settings.logError(err)
            res.status(400).send()
            return;
        })

        if (content) {
            res.status(200).type('.js').send(`
                document.open("text/html", "replace")
                document.write("<style>*{ overflow: -moz-scrollbars-none; overflow: hidden; } *::-webkit-scrollbar { width: 0 !important } .click_frame { cursor: pointer; position: absolute; z-index: 9999; width: 100%; height: 100%; left: 0; top: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; border: 1px solid #000; overflow: hidden; }</style>");
                document.write("${Helper.safeJavascript(content)}");
                document.write("<a target=\\"_blank\\"><div class=\\"click_frame\\" onClick=\\"window.open('${url}', '_blank')\\"></div></a>")
                document.close();
            `)
        }
    }
}

export async function getAdElement(req: Request, res: Response): Promise<void> {
    let data = await validateReqParams(req)
    let json = await validateJSONData(req)

    if (data.id && data.link && data.file) {
        let originalFile = `./files/${data.id}/${data.file}`
        let file = await getClosestElementByName(`./files/${data.id}/`, data.file)

        var options = {
            root: process.cwd() + '/',
            dotfiles: 'deny',
            headers: {
                'x-timestamp': Date.now(),
                'x-sent': true
            }
        };

        res.sendFile(file, options)
    }
}

async function getClosestElementByName(dir: string, filename: string) {
    let element = `${dir}${filename}`

    let files = fs.readdirSync(dir)
    files.forEach(function (file: string, index: number) {
        let r = Helper.getSafeRegexp(filename)
        if (r.test(file)) {
            element = `${dir}${file}`
            return element
        }
    })

    return element
}

async function getClosestHTML(dir: string) {
    let element = `${dir}index.html`

    let files = fs.readdirSync(dir)
    await files.forEach(function (file: string, index: number) {
        let r = new RegExp(/(.*).html/)
        if (r.test(file)) {
            element = `${dir}${file}`
            return element
        }
    })

    return element
}

export async function updateInfo(req: any, res: any) {
    let id: string = req.postData.id, desc: string = req.postData.description
    let update = DB.updateFields(`banners`, [`descricao`], [desc], id)
    let suc = false
    if (update) {
        suc = true
    }
    res.status(200).json({ success: suc })
}

export async function archiveItems(req: Request, res: Response): Promise<void> {
    if (!req.postData.items) {
        Settings.logError('Invalid item data for banners archive.')
        res.status(400).send()
    } else {
        let archive = await bannerRepository.archiveItems(req.postData.items)
        let redis = await redisRepository.deleteWhere('banners:*')
        res.status(200).json({ success: archive })
    }
}