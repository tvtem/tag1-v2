import * as Settings from '../settings';
//import * as authService from '../services/auth';
import { validateLogin, validateToken } from '../services/validators';
import { NextFunction, Request, Response } from 'express';
import {tokenRepository, userRepository} from '../services/database'

export async function login(req: Request, res: Response): Promise<void> {
    try {

        const { email, password } = await validateLogin(req);

        let user = await userRepository.findUserByEmail(email)
        if (user && user.password === password){
            let token = tokenRepository.generateToken(user.id)
            
           res.json({ auth: true, token: token, name: user.name  })
           return
        } 
        
        res.json({ auth: false })

    } catch (err) {
        Settings.logError(err)
        res.status(401).send();
    }
}

export async function validateUserToken(req: Request, res: Response, next:any): Promise<void> {
    try {

        const { token } = await validateToken(req);

        res.json({ authorized: true})

    } catch (err) {
        Settings.logError(err)
        res.status(401).send();
    }
}