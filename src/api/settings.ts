const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const files = new transports.File({ filename: 'api.log' });
const console = new transports.Console();

const myFormat = printf((info: any) => {
    return `${info.timestamp} [${info.level}]: ${info.message}`;
});

const LOGGER = createLogger({
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [
        console,
        files
    ]
});


export const logError = function (msg: any) {
    let stack = new Error().stack
    LOGGER.error(`${msg} - ${stack}`)
}

export const logInfo = function (msg: any) {
    LOGGER.info(`${msg}`)
}

export const API_SECRET = 'API_TEST_2018'

export const API_BASE_URL = 'http://localhost:8855/'