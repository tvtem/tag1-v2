
const nanoid = require('nanoid')
const generate = require('nanoid/generate')
import fs from 'fs';
import * as Settings from './settings';

export function generateSafeUniqueID() {
    return nanoid(64)
}

export function generateSaferUniqueID() {
    return generate('1234567890abcdefghijklmnopqrstuvwxyz_', 32)
}

export async function readFile(filename: any, encoding: any = 'utf8') {
    return await new Promise<any>((resolve, reject) => {
        try {
            fs.readFile(filename, encoding, function (err, data) {
                if (err) {
                    reject(err);
                } else
                    resolve(data);
            });
        }
        catch (err) {
            Settings.logError(err)
            reject('Could not read the file at ' + filename + '.')
        }
    });
}

export function safeJavascript(content: any) {
    content = content.replace(new RegExp('{{CLICK_TAG}}', 'g'), "")
    content = content.replace(new RegExp(';', 'g'), "; ")
    content = content.replace(/(\/\*([\s\S]*?)\*\/)|([^(?!:)]\/\/(.*)$)/gm, ' ');
    content = content.replace(/(\r\n|\n|\r)/gm, "\\n")
    content = content.replace(new RegExp('"', 'g'), '\\"');

    return content
}

export function getSafeRegexp(str: string) {
    let regexp = str
    let oregexp = regexp
    let jokers = `áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙãÃçÇüÜïÏâÂêÊîÎôÔûÛäÄëËöÖõÕñÑÿýÝ`
    let jk = jokers.split('').forEach((c: any, index: any) => {
        regexp = regexp.replace(new RegExp(c, 'g'), "(.*)")
    })

    return new RegExp(regexp, "g")
}

/**
 * Encodes multi-byte Unicode string into utf-8 multiple single-byte characters
 * (BMP / basic multilingual plane only).
 *
 * Chars in range U+0080 - U+07FF are encoded in 2 chars, U+0800 - U+FFFF in 3 chars.
 *
 * Can be achieved in JavaScript by unescape(encodeURIComponent(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} unicodeString - Unicode string to be encoded as UTF-8.
 * @returns {string} UTF8-encoded string.
 */
export function utf8Encode(unicodeString: string) {
    if (typeof unicodeString != 'string') throw new TypeError('parameter ‘unicodeString’ is not a string');
    const utf8String = unicodeString.replace(
        /[\u0080-\u07ff]/g,  // U+0080 - U+07FF => 2 bytes 110yyyyy, 10zzzzzz
        function (c) {
            var cc = c.charCodeAt(0);
            return String.fromCharCode(0xc0 | cc >> 6, 0x80 | cc & 0x3f);
        }
    ).replace(
        /[\u0800-\uffff]/g,  // U+0800 - U+FFFF => 3 bytes 1110xxxx, 10yyyyyy, 10zzzzzz
        function (c) {
            var cc = c.charCodeAt(0);
            return String.fromCharCode(0xe0 | cc >> 12, 0x80 | cc >> 6 & 0x3F, 0x80 | cc & 0x3f);
        }
    );
    return utf8String;
}

/**
 * Decodes utf-8 encoded string back into multi-byte Unicode characters.
 *
 * Can be achieved JavaScript by decodeURIComponent(escape(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} utf8String - UTF-8 string to be decoded back to Unicode.
 * @returns {string} Decoded Unicode string.
 */
export function utf8Decode(utf8String: string) {
    if (typeof utf8String != 'string') throw new TypeError('parameter ‘utf8String’ is not a string');
    // note: decode 3-byte chars first as decoded 2-byte strings could appear to be 3-byte char!
    const unicodeString = utf8String.replace(
        /[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,  // 3-byte chars
        function (c) {  // (note parentheses for precedence)
            var cc = ((c.charCodeAt(0) & 0x0f) << 12) | ((c.charCodeAt(1) & 0x3f) << 6) | (c.charCodeAt(2) & 0x3f);
            return String.fromCharCode(cc);
        }
    ).replace(
        /[\u00c0-\u00df][\u0080-\u00bf]/g,                 // 2-byte chars
        function (c) {  // (note parentheses for precedence)
            var cc = (c.charCodeAt(0) & 0x1f) << 6 | c.charCodeAt(1) & 0x3f;
            return String.fromCharCode(cc);
        }
    );
    return unicodeString;
}