import * as Settings from '../settings';

export function validateLogin(req: any) {
    return new Promise<{ email: string, password: string }>((resolve, reject) => {
        try {
            let rd = req.body;
            if (!rd || !rd.email || !rd.password) reject('Invalid data received.')

            resolve({ email: rd.email, password: rd.password });
        }
        catch (err) {
            Settings.logError(err)
            reject('Client sent invalid POST information to server.')
        }
    });
}

export function validateToken(req: any) {
    return new Promise<{ token: string }>((resolve, reject) => {
        try {
            let rd = req.query;
            if (!rd || !rd.token) reject('Invalid data received.')

            resolve({ token: rd.token });
        }
        catch (err) {
            Settings.logError(err)
            reject('Client sent invalid POST information to server.')
        }
    });
}

export function validateJSONData(req: any) {
    return new Promise<any>((resolve, reject) => {
        try {
            let rd = req.body;
            if (!rd) reject('Invalid data received.')
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err)
            reject('Client sent invalid POST information to server.')
        }
    });
}

export function validateGetData(req: any) {
    return new Promise<any>((resolve, reject) => {
        try {
            let rd = req.query
            if (!rd) reject('Invalid data received.')
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err)
            reject('Client sent invalid GET information to server.')
        }
    });
}

export function validateReqParams(req: any) {
    return new Promise<any>((resolve, reject) => {
        try {
            let rd = req.params

            if (!rd) reject('Invalid Req Param data received.')
            resolve(rd);
        }
        catch (err) {
            Settings.logError(err)
            reject('Client sent invalid POST information to server.')
        }
    });
}