import * as Settings from '../settings';
var mysql = require('mysql');
var RedisServer = require('ioredis');

import * as tokenRepository from '../database/token'
import * as userRepository from '../database/user'
import * as bannerRepository from '../database/banners'
import * as redisRepository from '../database/redis'


let Connection: any = null;

class Database {

    Redis: any = null;

    create() {
        try {
            let _conn: any = mysql.createPool({
                connectionLimit: 200,
                host: '35.230.107.215',
                user: 'tvtem',
                password: 'tvtem@!2019',
                database: 'tag1'
            });
            Connection = _conn;

            Connection.getConnection(function (err: any, connection: any) {
                if (err) {
                    Settings.logError('Database Error: ' + err.message)
                    process.exit()
                }

                if (connection) {
                    Settings.logInfo('Database connected successfully.')
                }
            });

            this.Redis = new RedisServer();
            if (this.Redis.set('online', true)) {
                Settings.logInfo(`Redis Server connected successfully (${this.Redis.get('online')}).`)
            }

        } catch (err) {
            Settings.logError(err)
        }

        return this;
    }

    destroy() {

    }

    async executeRaw(qry: string) {
        return await new Promise(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                try {
                    let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                        if (err) {
                            Settings.logError(err.sqlMessage);
                            connection.release();
                            resolve({ error: true });
                        } else {
                            connection.release();
                            resolve(JSON.parse(JSON.stringify(rows)));
                        }
                    });
                } catch (err) {
                    reject(err)
                }
            });

        });
    }

    async getName(table: string, id: number) {
        return await new Promise(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                let qry = `SELECT \`name\` FROM \`${table}\` WHERE id='${id}' `;
                let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                    if (err) {
                        Settings.logError(err.sqlMessage);
                        connection.release();
                        resolve('NULL');
                    } else {
                        connection.release();
                        let data = JSON.parse(JSON.stringify(rows))[0];
                        if (data === undefined)
                            resolve('')
                        else {
                            resolve(data.name);
                        }
                    }
                });
            });

        });
    }

    async getLastTableID(table: string) {
        return await new Promise<number>(function (resolve: any, reject: any) {
            Connection.getConnection(function (err: any, connection: any) {
                let qry = `SELECT \`id\` FROM \`${table}\` ORDER BY \`id\` DESC LIMIT 1`;
                let result = connection.query(qry, function (err: any, rows: any, fields: any) {
                    if (err) {
                        Settings.logError(err.sqlMessage);
                        connection.release();
                        resolve(0);
                    } else {
                        connection.release();
                        let data = JSON.parse(JSON.stringify(rows))[0];
                        if (data === undefined)
                            resolve(0)
                        else {
                            resolve(data.id);
                        }
                    }
                });
            });

        });
    }

    filterQuery(string: string) {
        let newString: string = `${string}`

        newString = newString.replace(new RegExp(`'`, 'g'), `\\'`)

        return newString
    }

    async updateFields(table: any, fields: any, values: any, id: any): Promise<any> {
        let now = Date.now();
        let data: any = null;

        fields.push('updated')
        values.push(now)

        if (id === "new") {
            fields.push('created')
            values.push(now)

            if (values.length < fields.length) return false;

            let f = '';
            let v = '';

            let ii = 0;
            for (let i = 0; i < fields.length; i++) {
                let commit = ii === 0 ? '' : ', ';

                f = f + commit + '`' + fields[i] + '`';
                v = v + commit + "'" + this.filterQuery(values[i]) + "'";

                ii++;
            }

            let withData = `(${f}) VALUES (${v})`;

            let qry = `INSERT INTO ${table} ${withData}`;
            let exec: any = await DB.executeRaw(qry);
            data = exec.insertId;
        }
        else {

            if (values.length < fields.length) return false;


            let withData = 'SET ';
            for (let i = 0; i < fields.length; i++) {
                let commit = i === 0 ? '' : ', ';
                withData = withData + commit + `\`${fields[i]}\`='` + this.filterQuery(values[i]) + "'";
            }
            let sql = `UPDATE ${table} ${withData} WHERE \`id\`='${id}'`
            data = await DB.executeRaw(sql);
        }

        if (!data)
            return values && values.length > 0 ? values[0] : false
        return data;
    }
}

let DB = new Database().create()
export { DB }
export { tokenRepository, userRepository, bannerRepository, redisRepository }
export default Database;